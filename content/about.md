---
title = "About"
date = 2020-12-14T21:12:44+01:00
tags = [""]
draft = false
toc = false
backtotop = false
---

I hold a bachelor’s and a master’s degree in Computer Science from the University of Oslo. I have been working as a developer, platform engineer and system administrator for four years.

## Key competency

- Platform (AWS, Azure)
- Architecture & Cloud (microservices, immutable infrastructure, DevOps, PaaS, SaaS, IaaS, IaC)
- Orchestration & Management (Consul, Nomad, Kubernetes, AKS, Docker Swarm, etcd, envoy, Nginx, haproxy, træfik, Apache)
- Provisioning (Terraform, Ansible, Packer, Vagrant, Foreman, puppet, openstack)
- Container Technology (Harbor, Docker, buildah, podman, contianerd)
- Continous Integration & Continous Deployment (Jenkins, GitLab)
- Data Integration (Kafka)
- Observability & Analysis (Splunk, Prometheus, Grafana, Icinga, Nagios, Elasticsearch, Logstash, Kibana)
- Security (Vault)
- NoSQL (Redis, MongoDB, CouchDB)
- Operations (VMware)
- Programming (GO, Python, Ruby, Java, Bash, C)

## Certifications

- [Certified AWS Developer - Associate](https://www.youracclaim.com/badges/a77adad6-aea9-406e-bce5-ceee58a0cccb/linked_in_profile)
- [Certified Kubernetes Administrator](https://www.youracclaim.com/badges/42c58c2b-68cd-46d1-a395-0f29d9f2f5f1/linked_in_profile)
- [Certified Terraform Associate](https://www.youracclaim.com/badges/b0800bc6-1465-460d-9c81-31bd91e8714e)

## Experience

After graduating university I started working as a consultant. During the last years I've worked in finance and government entities.

Skatteetaten

- Platform Engineer
- DevOps Engineer
- Scrum Master

Frende Forsikring

- Database Engineer

Computas

- System Administrator
