---
title: Book Review - The Phoenix Project
date: 2021-03-11
tags: ["book review", "devops"]
---

- [1. Introduction](#1-introduction)
- [2. Lean methodology](#2-lean-methodology)
- [3. Technical debt](#3-technical-debt)
- [4. DevSecOps](#4-devsecops)
- [5. Team structure](#5-team-structure)
- [6. Conclusion](#6-conclusion)

I recently read _The Phoenix Project: A Novel about It, Devops, and Helping Your Business Win_ written by Gene Kim, Kevin Behr and George Spafford. The book describes how Parts Unlimited transforms into a DevOps culture. We follow Bill, an Operations manager, as he tries to rescue the company from disaster.

![phoenix](/phoenix.jpg)

# 1. Introduction

The Phoenix Project is an easy to read novel. The characters are highly relatable and more than often you will have had similar real life experiences.

As this book was published around 8 years ago, many of the concepts outlined in the book has later been adopted by educational institutions and is taught as part of IT educations. Agile and Lean methodology is the industry standard, that said this book is great for refreshing fundamental lessons.

# 2. Lean methodology

Bill and his team does not keep track of incoming tasks. Therefore they need to visualize flow of work in the IT department. To illustrate this the authors describe how Toyota applied Kanban to improve manufacturing efficiency. Bill is thought this process by visiting the manufacturing plant and watching how inventory goes from ready to processed, and finally shipped to the customer. In this process Bill is able to identify where work gets introduced into the group, what the constraints are that prevent work from getting done and how work is delivered.

# 3. Technical debt

A key outcome from the book is how unplanned work is influencing planned work. By visualizing tasks being worked on, categorizing them into different services you are able to learn and keep unplanned work to a minimum. There are different sources of unplanned work like operational issues and technical debt. Reducing technical debt will instantly reduce amount of unplanned work.

# 4. DevSecOps

Inefficient communications between IT departments is a major problem in Parts Unlimited. A usual blocker for Bill and his team is the Security Group. There are always rules and regulations holding up projects. To achieve efficient collaboration every department needs to work toward a common goal. If Dev, Sec and Ops are separate entities with separate goals they will always prioritize differently and hold each other back. There has to be a balance between risk and growth. In a DevSecOps environment security is a shared responsibility integrated into application development and infrastructure operations from the start.

# 5. Team structure

The Phoenix Project portrays the different types of personalities you encounter in a team. Patty is the meticulous type and creates overly complex processes no one can follow, Wes says anything on his mind and Brent is the know it all without time to convey his knowledge. To achieve efficient teams you have to make sure the goals are aligned with how each person works.

# 6. Conclusion

I will highly recommend this book. It is fun and very recognizable, which makes you think about the processes in your own company. Reading this has helped me get a better understanding of how individual goals affect day to day interactions.
