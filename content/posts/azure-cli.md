---
title: Azure CLI
date: 2022-11-09
tags: ["Azure", "CLI"]
author: "Me"
showToc: true
TocOpen: true
---


# Azure CLI

[Azure CLI](https://learn.microsoft.com/en-us/cli/azure/) is a set of commands used to create and manage Azure resources.

To start off, run the following command to display all available commands.

```bash
az --help
```

To display available subcommands add ```--help``` after any command.

```bash
az login --help
```

Use the ```find``` command to fetch examples of use for commands.

```bash
az find "az login"
```

## Login

To login to Azure run the following command:

```bash
az login
```

To sign in with a service principal run the following command:

```bash
az login --service-principal -u <CLIENT_ID> -p <CLIENT_SECRET> --tenant <TENANT_ID>
```

If you have multiple tenants it can be useful to login to different tenants in each terminal window. This can be achieved with the ```--use-device-code``` argument. See [Azure docs](https://learn.microsoft.com/en-us/cli/azure/authenticate-azure-cli#sign-in-interactively).

```bash
az login --use-device-code --tenant <TENANT_ID>
```

## Automated tests

In a CI/CD environment Azure CLI could be used to automate several tasks. For example running pre flight checks before further pipelines are executed. Several Azure resources allow us to verify name is not in use already.

```bash
$ az acr check-name --name tests
{
  "message": "The registry tests is already in use.",
  "nameAvailable": false,
  "reason": "AlreadyExists"
}
```

```bash
$ az storage account check-name --name test
{
  "message": "The storage account named test is already taken.",
  "nameAvailable": false,
  "reason": "AlreadyExists"
}
```
