---
title: ReCAPTCHA in Staticman
date: 2021-12-30
tags: ["Staticman", "comments", "ReCAPTCHA"]
author: "Me"
showToc: true
TocOpen: true
---

In this post I will implement ReCAPTCHA with Staticman.

Due to Spam from bots we need to enable ReCAPTCHA to verify human interaction.

Go to Google [ReCAPTCHA](https://www.google.com/recaptcha/admin/create) and create one for your website. Choose verison 2 as Staticman only supports this verison.

Encrypt the secret key by going to your heroku web app.

```
https://staticman-erik.herokuapp.com/v3/encrypt/SECRET
```

Edit the config.yml file with:

```yml
staticman:
    api: https://staticman-erik.herokuapp.com/v3/entry/gitlab/eriksnartland/blog_comments/main/comments
    recaptcha:
    sitekey: "key"
    secret: "encoded-secret"
```

Edit the staticman.yml file in the blog_comments repo:

```yml
reCaptcha: 
    enabled: true
    sitekey: "key"
    secret: "encoded-secret"
```

Edit the html section of your website.

Add the following code to ```themes/PaperMod/assets/css/common/main.css```.

```css
.staticman-comments .g-recaptcha {
    padding: 0.5rem 0;
}
```

Edit the file ```themes/PaperMod/layouts/partials/staticman.html```.

```html
  <form class="js-form form" method="post" action="{{ $.Site.Params.staticman.api }}" onsubmit="return checkForm(this);">
    <fieldset>
        <input type="hidden" name="options[redirect]" value="{{ $.RelPermalink | absURL }}#comment-thankyou">
        <input type="hidden" name="options[slug]" value="{{ replace $.RelPermalink "/" "" }}">
        <input type="hidden" name="options[parent]" value="">
    
        {{ if $.Site.Params.staticman.recaptcha }}
        <input type="hidden" name="options[reCaptcha][siteKey]" value="{{ $.Site.Params.staticman.recaptcha.sitekey }}">
        <input type="hidden" name="options[reCaptcha][secret]"  value="{{ $.Site.Params.staticman.recaptcha.secret }}">
        {{ end }}
        <label for="fields[name]">Name:</label>
        <input name="fields[name]" id="yourname" type="text" placeholder="Your name"/>
        <label for="fields[comment]">Comment:</label>
        <textarea name="fields[comment]" id="yourcomment" placeholder="What are you thinking?"></textarea>
        {{ if $.Site.Params.staticman.recaptcha }}
            <div class="g-recaptcha" id="div-recaptcha" data-sitekey="{{ $.Site.Params.staticman.recaptcha.sitekey }}"></div>
        {{ end }}
        
        <output id="warningComment"></output>
        <button class="button" id="submitButton">Submit</button>
    </fieldset>
  </form>
```

Edit ```themes/PaperMod/layouts/partials/staticman-js-common.js```.

```js
var onloadCallbackCaptcha = function(sitekey) {
    grecaptcha.render('div-recaptcha');
  };

function showComments() {
    // Remove button
    var staticmanButton = document.getElementById('staticman-button');
    staticmanButton.parentNode.removeChild(staticmanButton); 
    // Un-hide comments
    var staticmanComments = document.getElementById('staticman-comments');
    staticmanComments.style.display = 'block'; 
    // load recaptcha script (delete if not using recaptcha)
    var script = document.createElement('script');
    script.setAttribute('src', 'https://www.google.com/recaptcha/api.js?onload=onloadCallbackCaptcha&render=explicit');
    document.head.appendChild(script);
}

function checkForm(form){
    if(typeof(grecaptcha) === "undefined"){
        form.warningComment.style.display = 'block'; 
        form.warningComment.innerText = "There was a problem loading the reCaptcha so it is not possible to submit a comment at the moment";
        return false;
    }
    else if(grecaptcha.getResponse() == ""){
        form.warningComment.style.display = 'block'; 
        form.warningComment.innerText = "Please confirm you are not a robot :)";
        return false;
    }
    else if(form.yourname.value == ""){
        form.warningComment.style.display = 'block'; 
        form.warningComment.innerText = "Please type your name";
        return false;
    }
    else if(form.yourcomment.value == ""){
        form.warningComment.style.display = 'block'; 
        form.warningComment.innerText = "Please type a comment";
        return false;
    }
    else{
        form.submitButton.disabled = true;
        form.warningComment.style.display = 'none'; 
        form.warningComment.innerText = "";
        form.submitButton.innerText = "Sending...";
        return true;
    }   
}
```
