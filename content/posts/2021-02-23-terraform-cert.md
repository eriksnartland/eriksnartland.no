---
title: How I passed the Terraform Associate Certificate
date: 2021-02-23
tags: ["terraform","certification", "iac"]
---

- [1. Infrastructure as Code](#1-infrastructure-as-code)
- [2. HashiCorp Terraform](#2-hashicorp-terraform)
- [3. Preparations for the exam](#3-preparations-for-the-exam)
- [4. Tips & Tricks](#4-tips--tricks)
- [5. Related Certificates](#5-related-certificates)

In february I passed the Terraform Associate Certificate making me a certified Cloud Engineer.

![cert](/terraform.png)

# 1. Infrastructure as Code

Infrastructure as code (IaC) is the process of managing and provisioning infrastructure. IaC allows versioned configuration of infrastructure which can be deployed in minutes. Where dev environments usually would take hours of manual configuration by multiple Ops people to set up, IaC allows developers to supply fully configured test, dev and prod environments.

Some advantages of IaC includes consistency, speed and efficiency. Having the ability to always implement incremental changes to your infrastructure and environments generates higher productivity for development. Some technologies enabling IaC includes: Kubernetes, Terraform, Ansible, Puppet, etc.

# 2. HashiCorp Terraform

I've been using HashiCorp products for about 5 years. I was first introduced to Consul back in 2016 during my studies. Consul offer automated service networking in the cloud. In the HashiCorp ecosystem Terraform aims to automate provisioning, compliance and management of infrastructure. Terraform is a tool that allows you to define infrastructure in human and machine-readable code. It gives a blueprint of the underlying infrastructure and helps managing resources cross platform.

# 3. Preparations for the exam

The resources I used consists of official HashiCorp documentation and practice exams on Udemy. In the last two years I've had the opportunity to work with Terraform and other HashiCorp products on a daily basis. This has given me valuable experience and understanding of DevSecOps and IaC concepts. Even though you have a few years of experience with Terraform going through the HashiCorp Certificate resources can be super useful. It is a thorough course and covers all aspects of the exam.

- Terraform
  - [Study Guide](https://learn.hashicorp.com/tutorials/terraform/associate-study?in=terraform/certification)
  - [Exam Review](https://learn.hashicorp.com/tutorials/terraform/associate-review?in=terraform/certification)
  - [Sample Questions](https://learn.hashicorp.com/tutorials/terraform/associate-questions?in=terraform/certification)

To verify my knowledge and prepare for the exam I also took some practice exams on Udemy. Aim for an 80 % passing score on each practice exam, and you are ready for the real deal.

- [Udemy practice exams](https://www.udemy.com/course/terraform-associate-practice-exam/)

# 4. Tips & Tricks

A colleague introduced me to [terraform-docs](https://github.com/terraform-docs/terraform-docs), which is a utility to generate documentation from Terraform modules. It is very useful to standardize documentation of your modules.

# 5. Related Certificates

If you are interested in cloud computing and platform engineering I highly recommend looking into getting certified in:

- Kubernetes
  - [CKA](https://www.cncf.io/certification/cka/)
  - [CKAD](https://www.cncf.io/certification/ckad/)
- Cloud provider
  - [Azure](https://docs.microsoft.com/en-us/learn/certifications/azure-developer/)
  - [AWS](https://aws.amazon.com/certification/certified-developer-associate/)
  - [GCP](https://cloud.google.com/certification/cloud-developer)
- HashiCorp
  - [Vault](https://www.hashicorp.com/certification/vault-associate)
  - [Consul](https://www.hashicorp.com/certification/consul-associate)

These will all improve your skills and increase your value in the market.
