---
title: Static comments with Staticman
date: 2021-11-28
tags: ["Staticman", "comments", "disqus"]
author: "Me"
showToc: true
TocOpen: true
cover:
  image: "gitops.jpg"
  alt: "alt"
  caption: "caption"
  relative: false # To use relative path for cover image, used in hugo Page-bundles
---

My website is built with [Hugo](https://gohugo.io), which is a fast, open-source framework, that enables building flexible websites. To engage with audiences I have enabled comments on my site.

# Disqus

[Disqus](https://disqus.com/) is a third party application which ships with Hugo. Disqus allows users to comment on blog posts. It is easy to setup and is supported by most Hugo themes. To enable Disqus add the following line in your config:

```yaml
disqusShortname: yourdiscussshortname
```

## Why is it bad?

Disqus makes money by selling user data. The user data is supplied by your users and their activity. The entire business model of Disqus is based on ad revenue. When Disqus is enabled on your website you allow them to track all traffic. The information is used for targeted ads on your users.

## Bloated software

Since Disqus is an ad company disguised as comment software their services becomes bloated. As a result performance takes a hit and loading pages is slow.

## GDPR

To enable Disqus we also need to add cookies to the site. This is because user information is tracked and sold to third parties. See [post](https://www.iubenda.com/en/help/23947-disqus-gdpr-how-to-be-compliant) for more information.

[Datatilsynet](https://www.datatilsynet.no/en/) is actually looking to fine Disqus with [NOK 25 000 000 for not complying with the GDPR rules on accountability, lawfulness, and transparency](https://www.datatilsynet.no/en/news/2021/intent-to-issue--25-million-fine-to-disqus-inc/).

I don't want to sell user information to third party apps like Disqus and to be honest there is no point. Comments were supposed to add engagement and be a simple way for users to interact with the blog.

# Alternatives

Now that we have established Disqus as an evil corporation looking to sell your information we need to look at alternatives to enable comments.

## Issues

When choosing a new comments provider I want to look at the following:

- Where are the comments stored?
- No tracking
- GDPR
- Performance
- Simplicity

When using third party apps like Disqus the comments and user information is stored on their servers.

Hugo lists these alternatives to Disqus:

- Cactus Comments (Open Source, Matrix appservice, Docker install)
- Commento (Open Source, available as a service, local install, or docker image)
- Graph Comment
- Hyvor Talk (Available as a service)
- IntenseDebate
- Isso (Self-hosted, Python) (tutorial)
- Muut
- Remark42 (Open source, Golang, Easy to run docker)
- Staticman
- Talkyard (Open source, & serverless hosting)
- Utterances (Open source, GitHub comments widget built on GitHub issues)

These are all good alternatives with different approaches to add a website comments section. My choice fell on Staticman as it does not store any user information and all comments are stored in git.

# Staticman

Staticman is an Open Source application which enables comments on static websites. While many comment solutions for static websites require embedding arbitrary JavaScript (and often privacy invading trackers), Staticman works with a simple REST form submission.

The technical implementation of Staticman will be posted in another blog.

# Sources

Writing this post I used the following sources:

- https://fatfrogmedia.com/delete-disqus-comments-wordpress/ 
- https://www.iubenda.com/en/help/23947-disqus-gdpr-how-to-be-compliant
- https://gohugo.io/content-management/comments/
- https://pakstech.com/blog/hugo-comments/
- https://nrkbeta.no/2019/12/18/disqus-delte-persondata-om-titalls-millioner-internettbrukere-uten-at-nettsidene-visste-om-det/
- https://itavisen.no/2020/01/08/om-disqus-og-gdpr-pa-itavisen/ 