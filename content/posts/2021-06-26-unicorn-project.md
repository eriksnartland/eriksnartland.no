---
title: Book Review - The Unicorn Project
date: 2021-06-26
tags: ["book review", "devops"]
# weight: 1
# aliases: ["/first"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
---

_The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data_ written by Gene Kim was published in the end of 2019. It is a follow up on The Phoenix Project being told from a developer's point of view as Parts Unlimited transforms into a DevOps culture.

![unicorn](/unicorn.jpg)

# 1. Introduction

Maxine is a senior lead developer and architect at Parts Unlimited. She has many years of experience in the company and has worked on some of the most advanced systems. After making a mistake with a payroll application she is punished and moved to a different project called the Unicorn project. Maxine experiences an uncaring bureaucratic system where no one are able to make changes without getting sign offs from managers. Developers are forced to synchronize their changes making progress slow as all systems are dependant of each other. Parts Unlimited is cursed with siloed departments, over complicated change processes and a blameful culture.

# 2. Large Organizations and Legacy Code

Being moved to a different project is discouraging and demoralizing. She has invested time and energy into her previous project and feels badly treated. She now has to start all over in a foreign environment and make new friends. The Unicorn project is years behind schedule and her introduction to the new team is rough. The project is struggling and employees have been leaving. The management is chaotic and instead of prioritizing building a strong foundation the managers have been focused on feature releases to make dependant external projects happy. This has resulted in a code base that suffers from technical debt.

Making matters worse, dev, test and ops are separate entities that do not work together. When a developer has "finished" a feature they hand off the code to the test team who in turn hands it over to operations. After discovering this Maxine is worried, after all, how can an operations team effectively operate a system that they don't have any knowledge of, and how can developers help create a robust and effective system if they don't know how to operate it. As it turns out the developers only work with small sections of the system at a time, never compiling the system as a whole. As a result many feature requests don't pass the tests and are returned to dev for further development. The process is slow and the system is prone to often failures.

The Unicorn Project show that even senior developers struggle with legacy systems and bad developer environments. Having worked on many projects Maxine know the key to productive developers is a good developer experience.

# 3. Developer Experience

In order to meet the requirements of senior management the best developers are put to develop new features. As a result the development process is slow and the whole team is suffering with low productivity. To increase productivity an effective developer experience is necessary. Reducing time spent waiting for dev environments and slow builds should be used developing new features.

Feature changes usually have a short time investment to implement and results are easy to see and measure. Tasks that involve improving developer experiences are often deprioritized when a project is late on schedule making the matters worse. In order to turn the tide you have to invest in long term improvements in order to make your developers more productive. By improving developer environments, build tools and test automation the quality, robustness, scalability and performance of the overall system will improve.

To summarize, a happy developer is a productive developer. A happy developer require easily set up dev environments, automated tests and psychological safety.

# 4. Red Shirts

As the book progress we see Maxine joining a group of passionate employees with the purpose of changing the company for the better. They call themselves the red shirts and act as a rebel alliance. The red shirts consists of a group of workaholics, showing undeserved loyalty to a company that has been a poor employer to them. In the real world they would all quit and get a job somewhere else. The book also plays into the american approach of glorifying long work days and giving it all at work. The work-life balance is out of proportion and none of them value personal time over work. This makes the whole concept of red shirts saving the company looking like something out of a dystopian society rather than a fun approach to implement better engineering practices in a large organization.

Reading the book makes me appreciate 8 hour work days and the union employee rights we take for granted.

# 5. Ideals

In the book we are presented with five ideals:

- Locality and Simplicity
- Focus, Flow and Joy
- Improvement of Daily Work
- Psychological Safety
- Customer Focus

Locality and simplicity relates to the degree of which changes can be made to an application without affecting various other teams and applications. By uncoupling applications we reduce complexity and encourage psychological safety of developers that changes they make to a single application do not break the entire system. This increases developer productivity, focus and flow which in turn results in joy. Improvement of daily work addresses the importance of reducing technical debt. Technical debt lowers your team's agility, produce poor quality code and strain your testing team. A blameful culture may encourage developers to hide mistakes because of fair rather than being honest and fixing problems. An open blame free environment increases psychological safety and productivity. Customer focus is the ideal that to be able to deliver customer satisfactory it is important to prioritize the backend systems of the organization as well. It doesn't matter how fancy your customer facing front end applications are without a working backend.
